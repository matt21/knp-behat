# BDD, Behat, Mink and other Wonderful ThingsRESTing with Symfony

Well hi there! This repository holds the code and script
for the [Behat PHP course on KnpUniversity](https://knpuniversity.com/screencast/behat).

## Setup the Project

Ok, cool - this will be easy!

1. Make sure you have [Composer installed](https://getcomposer.org/).

2. In a terminal, move into the project, then install the composer dependencies:

```bash
composer install
```

Or you may need to run `php composer.phar install` - depending on *how*
you installed Composer. This will probably ask you some questions
about your database (answer for your system) and other settings
(just hit enter for these).

3. Load up your database

This project uses an Sqlite database, which normally is supported by PHP
out of the box.

To load up your database file, run:

```bash
php app/console doctrine:database:create
php app/console doctrine:schema:update --force
php app/console doctrine:fixtures:load
```

This will create - and populate - an `app/app.db` file.

4. Start up the built-in PHP web server:

```bash
php app/console server:run
```

Then find the site at http://localhost:8000.

You can login with:

user: admin
pass: admin

Have fun!

**BEHAT BUNDLES installation**

1st You have to install several bundles tat will handle behat and mink tests:

composer require behat/mink-extension --dev behat/mink-goutte-driver --dev

to run behat tests use command ./vendor/bin/behat 

first run run t with ./vandor/bin/behat --init  

this will create directiories and will setup the bundles for You


**PHPUNIT**

Last thing that isuseful is phpunit assertions:

composer require phpunit/phpunit --dev

And then require phpunit assertions in FeatureContext.php file

require_once __DIR__.'/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

**SELENIUM installation**

https://www.npmjs.com/package/selenium-standalone

Commands to install and run selenium with drivers

npm install selenium-standalone@latest -g
selenium-standalone install
selenium-standalone start


Example behat.yml file, You can also change default browser here

default:
 paths:
  features: features
  bootstrap: %behat.paths.features%/bootstrap
 extensions:
  Behat\MinkExtension\Extension:
    base_url:  'path-to-my-site'
    default_session: selenium2
    browser_name: 'chrome'
    goutte: ~
    selenium2:
        wd_host: "http://127.0.0.1:4444/wd/hub"
