Feature: ls
  In order to see the directory structure
  As a UNIX user
  I need to be able to list current directory's structure

  Background:
    Given there is a file named "john"

  Scenario: List 2 files in the directory
    Given there is a file named "hammond"
    When I run "ls"
    Then I should see "john" in the output
    And I should see "hammond" in the output

  Scenario: List one file and one directory
    Given there is a dir named "ingen"
    When I run "ls"
    Then I should see "john" in the output
    And I should see "ingen" in the output
