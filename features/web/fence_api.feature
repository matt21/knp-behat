Feature: Remote control Fence API
  In order to control fence security from anywhere
  As an API user
  I need to be able to POST JSON instructions to turn the fence on/off