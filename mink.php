<?php

require __DIR__.'/vendor/autoload.php';

use Behat\Mink\Driver\GoutteDriver;
use Behat\Mink\Session;
use Behat\Mink\Driver\Selenium2Driver;


$driver = new GoutteDriver();
$driver = new Selenium2Driver('chromium');

$session = new Session($driver);
$session->start();
$session->visit('http://jurassicpark.wikia.com');
var_dump($session->getCurrentUrl());

// DocumentElement
$page = $session->getPage();
var_dump(substr($page->getText(), 0, 75));

//Node element
$header = $page->find('css', '.wds-community-header__sitename a');
var_dump($header->getText());

$nav = $page->find('css', '.wds-tabs');
$linkEl = $nav->find('css', 'li a');
$linkEl = $page->findLink('Books');
var_dump($linkEl->getAttribute('href'));

$linkEl->click();

var_dump($session->getCurrentUrl());

$session->stop();


